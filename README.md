# TimeFactory

Static functions to retrieve current time anywhere without any dependency injection, but with a back-door for tests to
fixate those values.  
Because time is a cross-cutting concern, and using dependency injection for it involves too much boilerplate.

No dependencies outside what java 8 offers.  

If you're familiar with joda-time, it offered a similar functionality 
via `DateTimeUtils.setCurrentMillisProvider` and `DateTimeUtils.setCurrentTimeMillis`.  
Except this library does it via `TimeFactory.set*` and `reset` methods.  

## Adding the library to project

Click the badge below, and you'll find the latest version and instructions on how to add the library.

[![Maven Central](https://img.shields.io/maven-central/v/com.gitlab.ts14ic/time-factory.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.gitlab.ts14ic%22%20AND%20a:%22time-factory%22)

### Maven

```xml
<dependency>
    <groupId>com.gitlab.ts14ic</groupId>
    <artifactId>time-factory</artifactId>
    <version>{latestVersion}</version>
</dependency>
```

### Gradle

```groovy
dependencies {
    implementation 'com.gitlab.ts14ic:time-factory:{latestVersion}'
}
```

## Full description

The same description and examples can be found on the `TimeFactory` class.  

`TimeFactory` is a set of test-friendly time-retrieval functions that don't require any dependency injection.

Tests can override current clock/time using `TimeFactoryUtils` and its different setter methods.

`TimeFactory` uses a UTC clock by default, to avoid any unexpected issues like when the server timezone changes, 
and suddenly everything breaks.

To use `TimeFactory`, replace your calls to `now()` of `java.time` classes with calls to `now()`, `nowMillis()` 
etc of the `TimeFactory`.

For example:
```java
class Demo {
    void demo(String userOffset, String userTimeZone) {
        Instant instant = TimeFactory.now(); // instead of Instant.now();
        long now = TimeFactory.nowMillis();  // instead of System.currentTimeMillis();
        OffsetDateTime odt = TimeFactory.nowOffset(userOffset); // instead of Instant.now().atOffset(userOffset);
        ZonedDateTime zdt = TimeFactory.nowZoned(userTimeZone); // instead of Instant.now().atZone(userTimeZone);
    }

    // As a default value for fields
    class Person {
        private Instant createdAt = TimeFactory.now();
        // getters/setters
    }
}
```

Override the current time in tests using `TimeFactoryUtils.set*` methods.
Reset the clock back a normal ticking with `reset()`.

```java
import md.ts14ic.time.TimeFactoryUtils;

class Test {
    @BeforeEach
    void beforeEach() {
        TimeFactoryUtils.reset();
    }

    @Test
    void someTestThatUsesTime() {
        Instant now = TimeFactoryUtils.setNow(Instant.parse("2020-06-01T12:34:00.123Z"));
        // or Instant now = TimeFactoryUtils.freezeNow();
        // or Instant now = TimeFactoryUtils.setNow("2020-06-01 12:34:00.123Z");       
        // or Instant now = TimeFactoryUtils.setNow(OffsetDateTime.now());       
        // or Instant now = TimeFactoryUtils.setNow(ZonedDateTime.now());

        // Time is frozen now
        assertThat(TimeFactory.now()).isEqualTo(now);

        long personId = someService.createSomePerson();
        Person person = personRepository.getById(personId);

        assertThat(person.getCreatedAt()).isEqualTo(now);
        // or assertThat(person.getCreatedAt()).isEqualTo(TimeFactory.now());
    }

    @Test
    void someTestThatThatWantsMultipleNows() {
        // Same values are accepted by withNow, as by setNow above.
        TimeFactoryUtils.usingNow(Instant.parse("2020-06-01T12:34:00.123Z"), now -> {
            // TimeFactory.now() here returns same value as `now` parameter
        });
        // TimeFactory.now() here returns same values as before `usingNow()`
    }

    @Test
    void someTestThatUsesCurrentDate() {
        TimeFactoryUtils.setNow("2021-02-20 23:00:00Z");

        Person person = new Person("John", "Doe", LocalDate.of("1980-02-21"));
        
        // Internally calls TimeFactory.today("Europe/Berlin"), 
        // which returns 2021-02-21
        assertThat(birthdayService.mustGreet(person)).isTrue();
    }
}
```
