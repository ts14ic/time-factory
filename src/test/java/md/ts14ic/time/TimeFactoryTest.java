package md.ts14ic.time;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class TimeFactoryTest {

    @BeforeEach
    void beforeEach() {
        TimeFactoryUtils.reset();
    }

    @Test
    void test_withoutSetting_now_identicalToInstantNow() {
        // ARRANGE
        Instant nowFromInstant = Instant.now();

        // ACT
        Instant nowFromFactory = TimeFactory.now();

        // ASSERT
        assertThat(nowFromInstant).isCloseTo(nowFromFactory, within(1, ChronoUnit.SECONDS));
    }

    @Test
    void test_withoutSetting_nowMillis_identicalToCurrentTimeMillis() {
        // ARRANGE
        long nowFromSystem = System.currentTimeMillis();

        // ACT
        long nowFromFactory = TimeFactory.nowMillis();

        // ASSERT
        assertThat(nowFromSystem).isCloseTo(nowFromFactory, within(1000L));
    }

    @Test
    void test_now_usesSetClock() {
        // ARRANGE
        // Note: The Offset here is given for the sake of the test. Prefer UTC clocks.
        TimeFactoryUtils.setClock(Clock.fixed(Instant.parse("2020-06-01T12:34:00.123Z"), ZoneId.of("+2")));

        // ACT
        sleep();
        Instant now = TimeFactory.now();

        // ASSERT
        assertThat(now).isEqualTo(Instant.parse("2020-06-01T12:34:00.123Z"));
    }

    @SuppressWarnings("java:S2925") // Not waiting for async code. Just progressing the real clock.
    private void sleep() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_now_usesSetClockFromText() {
        return Stream.of(
                arguments("2020-09-01 12:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01 12:00:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01 12:00:00.123Z", Instant.parse("2020-09-01T12:00:00.123Z")),
                arguments("2020-09-01T12:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01T12:00:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01T12:00:00.123Z", Instant.parse("2020-09-01T12:00:00.123Z")),
                arguments("2020-09-01 12:00+05:30", Instant.parse("2020-09-01T06:30:00Z")),
                arguments("2020-09-01 12:00:00-02:00", Instant.parse("2020-09-01T14:00:00Z")),
                arguments("2020-09-01 12:00:00.123-04:00", Instant.parse("2020-09-01T16:00:00.123Z"))
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_now_usesSetClockFromText(String input, Instant expectedNow) {
        // ARRANGE
        Instant parsedInstant = TimeFactoryUtils.setNow(input);

        // ACT
        sleep();
        Instant now = TimeFactory.now();

        // ASSERT
        assertThat(now).as("now()").isEqualTo(expectedNow);
        assertThat(parsedInstant).as("parsed").isEqualTo(expectedNow);
    }

    @Test
    void test_now_usesSetNow() {
        // ARRANGE
        Instant expectedNow = Instant.ofEpochSecond(0);
        Instant convertedInstant = TimeFactoryUtils.setNow(expectedNow);

        // ACT
        sleep();
        Instant now = TimeFactory.now();

        // ASSERT
        assertThat(now).as("now()").isEqualTo(expectedNow);
        assertThat(convertedInstant).as("converted").isEqualTo(expectedNow);
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_setNow_convertsToInstant() {
        return Stream.of(
                arguments(
                        Instant.parse("2021-12-31T12:00:00Z"),
                        Instant.parse("2021-12-31T12:00:00Z")),
                arguments(
                        OffsetDateTime.parse("2021-12-31T12:00:00+02:00"),
                        /*   */Instant.parse("2021-12-31T10:00:00Z")
                ),
                arguments(
                        ZonedDateTime.parse("2021-12-31T12:00:00+01:00[Europe/Berlin]"),
                        /*  */Instant.parse("2021-12-31T11:00:00Z")
                )
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_setNow_convertsToInstant(TemporalAccessor nowAccessor, Instant expectedNow) {
        Instant convertedInstant = TimeFactoryUtils.setNow(nowAccessor);

        assertThat(convertedInstant).isEqualTo(expectedNow);

        sleep();
        assertThat(TimeFactory.now()).isEqualTo(expectedNow);
    }

    @Test
    void test_nowOffset_usesClockAndOffset() {
        // ARRANGE
        Instant parsedInstant = TimeFactoryUtils.setNow("2020-09-01 12:00:00Z");

        // ACT
        sleep();
        OffsetDateTime now = TimeFactory.nowOffset(ZoneOffset.of("+05:30"));

        // ASSERT
        assertThat(now).hasToString("2020-09-01T17:30+05:30");
        assertThat(parsedInstant).isEqualTo(now.toInstant());
    }

    @Test
    void test_nowOffsetWithId_usesClockAndOffset() {
        // ARRANGE
        Instant parsedInstant = TimeFactoryUtils.setNow("2020-09-01 12:00:00Z");

        // ACT
        sleep();
        OffsetDateTime now = TimeFactory.nowOffset("+03:00");

        // ASSERT
        assertThat(now).hasToString("2020-09-01T15:00+03:00");
        assertThat(parsedInstant).isEqualTo(now.toInstant());
    }

    @Test
    void test_nowZoned_usesClockAndZone() {
        // ARRANGE
        Instant parsedInstant = TimeFactoryUtils.setNow("2020-09-01 12:00:00Z");

        // ACT
        sleep();
        ZonedDateTime now = TimeFactory.nowZoned(ZoneId.of("Europe/Berlin"));

        // ASSERT
        assertThat(now).hasToString("2020-09-01T14:00+02:00[Europe/Berlin]");
        assertThat(parsedInstant).isEqualTo(now.toInstant());
    }

    @Test
    void test_nowZonedWithId_usesClockAndZone() {
        // ARRANGE
        Instant parsedInstant = TimeFactoryUtils.setNow("2020-09-01 12:00:00Z");

        // ACT
        sleep();
        ZonedDateTime now = TimeFactory.nowZoned("America/New_York");

        // ASSERT
        assertThat(now).hasToString("2020-09-01T08:00-04:00[America/New_York]");
        assertThat(parsedInstant).isEqualTo(now.toInstant());
    }

    @Test
    void test_withNow_temporalNow() {
        Instant outerNow = TimeFactoryUtils.freezeNow();

        sleep();
        Instant[] innerNows = new Instant[2];
        TimeFactoryUtils.usingNow(outerNow.minusSeconds(100), innerNow -> {
            innerNows[0] = innerNow;
            innerNows[1] = TimeFactory.now();
        });

        assertThat(TimeFactory.now())
                .as("outer restored")
                .isEqualTo(outerNow);
        assertThat(Stream.of(innerNows).distinct())
                .as("inner now")
                .containsExactly(outerNow.minusSeconds(100));
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_withNow_textNow() {
        return Stream.of(
                arguments("2020-09-01 12:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01 12:00:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01 12:00:00.123Z", Instant.parse("2020-09-01T12:00:00.123Z")),
                arguments("2020-09-01T12:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01T12:00:00Z", Instant.parse("2020-09-01T12:00:00Z")),
                arguments("2020-09-01T12:00:00.123Z", Instant.parse("2020-09-01T12:00:00.123Z")),
                arguments("2020-09-01 12:00+05:30", Instant.parse("2020-09-01T06:30:00Z")),
                arguments("2020-09-01 12:00:00-02:00", Instant.parse("2020-09-01T14:00:00Z")),
                arguments("2020-09-01 12:00:00.123-04:00", Instant.parse("2020-09-01T16:00:00.123Z"))
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_withNow_textNow(String nowText, Instant expectedInnerNow) {
        Instant outerNow = TimeFactoryUtils.freezeNow();

        sleep();
        Instant[] innerNows = new Instant[2];
        TimeFactoryUtils.usingNow(nowText, innerNow -> {
            innerNows[0] = innerNow;
            innerNows[1] = TimeFactory.now();
        });

        assertThat(TimeFactory.now())
                .as("outer restored")
                .isEqualTo(outerNow);
        assertThat(Stream.of(innerNows).distinct())
                .as("inner now")
                .containsExactly(expectedInnerNow);
    }

    @Test
    void test_clock_returnsSetClock() {
        Clock fixed = Clock.fixed(Instant.parse("2020-06-01T12:34:00.123Z"), ZoneId.of("Europe/Paris"));
        TimeFactoryUtils.setClock(fixed);
        sleep();
        assertThat(TimeFactory.clock()).isSameAs(fixed);

        TimeFactoryUtils.reset();
        assertThat(TimeFactory.clock()).isEqualTo(Clock.systemUTC());
    }

    @Test
    void test_freezeNow() {
        Instant frozenNow = TimeFactoryUtils.freezeNow();

        sleep();
        Instant now = TimeFactory.now();

        assertThat(now)
                .isEqualTo(frozenNow)
                .isNotEqualTo(Instant.now());
    }

    @Test
    void test_freezeNow_idempotency() {
        Instant now1 = TimeFactoryUtils.freezeNow();
        Instant now2 = TimeFactoryUtils.freezeNow();
        Instant now3 = TimeFactoryUtils.freezeNow();
        sleep();

        assertThat(Stream.of(now1, now2, now3).distinct())
                .containsExactly(TimeFactory.now())
                .doesNotContain(Instant.now());
    }

    @Test
    void test_freezeNow_truncated() {
        Instant frozenNow = TimeFactoryUtils.freezeNow(ChronoUnit.SECONDS);

        sleep();
        Instant now = TimeFactory.now();

        assertThat(now)
                .isEqualTo(frozenNow)
                .isNotEqualTo(Instant.now());
        assertThat(now.get(ChronoField.NANO_OF_SECOND))
                .as("no milli, micro or nano seconds")
                .isZero();
    }

    @Test
    void test_freezeNow_truncated_idempotency() {
        Instant now1 = TimeFactoryUtils.freezeNow(ChronoUnit.SECONDS);
        Instant now2 = TimeFactoryUtils.freezeNow(ChronoUnit.SECONDS);
        Instant now3 = TimeFactoryUtils.freezeNow(ChronoUnit.SECONDS);
        sleep();

        assertThat(Stream.of(now1, now2, now3).distinct())
                .containsExactly(TimeFactory.now())
                .doesNotContain(Instant.now());
        assertThat(TimeFactory.now().get(ChronoField.NANO_OF_SECOND))
                .as("no milli, micro or nano seconds")
                .isZero();
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_todayWithZoneId() {
        return Stream.of(
                arguments((Runnable) TimeFactoryUtils::reset, ZoneId.systemDefault(), LocalDate.now()),
                arguments((Runnable) () -> TimeFactoryUtils.setNow("2021-12-31T23:00:00Z"),
                        ZoneOffset.UTC, LocalDate.of(2021, 12, 31)),
                arguments((Runnable) () -> TimeFactoryUtils.setNow("2021-12-31T23:00:00Z"),
                        ZoneId.of("Europe/Berlin"), LocalDate.of(2022, 1, 1))
        );
    };

    @ParameterizedTest
    @MethodSource
    void test_todayWithZoneId(Runnable timeFreeze, ZoneId inZone, LocalDate expectedDate) {
        timeFreeze.run();

        LocalDate today = TimeFactory.today(inZone);

        assertThat(today).isEqualTo(expectedDate);
    }

    @SuppressWarnings("unused")
    static Stream<Arguments> test_todayWithZoneIdName() {
        return Stream.of(
                arguments((Runnable) () -> TimeFactoryUtils.setNow("2021-12-31T23:00:00Z"),
                        "UTC", LocalDate.of(2021, 12, 31)),
                arguments((Runnable) () -> TimeFactoryUtils.setNow("2021-12-31T23:00:00Z"),
                        "Europe/Berlin", LocalDate.of(2022, 1, 1))
        );
    };

    @ParameterizedTest
    @MethodSource
    void test_todayWithZoneIdName(Runnable timeFreeze, String inZone, LocalDate expectedDate) {
        timeFreeze.run();

        LocalDate today = TimeFactory.today(inZone);

        assertThat(today).isEqualTo(expectedDate);
    }
}
