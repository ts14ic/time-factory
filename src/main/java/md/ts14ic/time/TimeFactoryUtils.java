package md.ts14ic.time;

import java.time.Clock;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Test-only methods to modify time returned by {@link TimeFactory}.
 * <p>
 * Override the current time in tests using {@link #setNow(String)}, {@link #setNow(TemporalAccessor)}
 * or {@link #setClock(Clock)}.
 * Reset the clock back a normal ticking with {@link #reset()}.
 * <pre>{@code
 *      \@BeforeEach
 *      void beforeEach() {
 *          TimeFactoryUtils.reset();
 *      }
 *
 *      \@Test
 *      void someTestThatUsesTime() {
 *          Instant now = TimeFactoryUtils.setNow(Instant.parse("2020-06-01T12:34:00.123Z"));
 *          // or Instant now = TimeFactoryUtils.freezeNow();
 *          // or Instant now = TimeFactoryUtils.setNow("2020-06-01 12:34:00.123Z");
 *          // or Instant now = TimeFactoryUtils.setNow(OffsetDateTime.now());
 *          // or Instant now = TimeFactoryUtils.setNow(ZonedDateTime.now());
 *
 *          // Time is frozen now
 *          assertThat(TimeFactory.now()).isEqualTo(now);
 *
 *          long personId = someService.createSomePerson();
 *          Person person = personRepository.getById(personId);
 *      }
 *
 *      \@Test
 *      void someTestThatThatWantsMultipleNows() {
 *          // Same values are accepted by withNow, as by setNow above.
 *          TimeFactoryUtils.usingNow(Instant.parse("2020-06-01T12:34:00.123Z"), now -> {
 *              // TimeFactory.now() here returns same value as `now` parameter
 *          });
 *          // TimeFactory.now() here returns same values as before `usingNow()`
 *      }
 *
 *      \@Test
 *      void someTestThatUsesCurrentDate() {
 *          TimeFactoryUtils.setNow("2021-02-20 23:00:00Z");
 *          Person person = new Person("John", "Doe", LocalDate.of("1980-02-21"));
 *
 *          // Internally calls TimeFactory.today("Europe/Berlin"),
 *          // which returns 2021-02-21
 *          assertThat(birthdayService.mustGreet(person)).isTrue();
 *      }
 *  }</pre>
 *
 * @see TimeFactory
 */
public class TimeFactoryUtils {

    private static volatile Clock clock = Clock.systemUTC();
    private static final List<DateTimeFormatter> FORMATS;

    static {
        List<DateTimeFormatter> formats = new ArrayList<>();
        formats.add(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm[:ss[.SSS]]XXX"));
        formats.add(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm[:ss[.SSS]]XXX"));
        FORMATS = Collections.unmodifiableList(formats);
    }

    /**
     * Forces the factory to use the given clock - for tests only.
     *
     * @param clock The clock to be used by this factory.
     * @see Clock#fixed(Instant, ZoneId)
     * @see Clock#offset(Clock, Duration)
     * @see Clock#tick(Clock, Duration)
     **/
    public static void setClock(Clock clock) {
        TimeFactoryUtils.clock = Objects.requireNonNull(clock, "clock must be non-null");
    }

    /**
     * Returns the currently set clock.
     * <p>
     * For internal use by {@link TimeFactory}.
     *
     * @return currently set clock.
     */
    static Clock clock() {
        return clock;
    }

    /**
     * Fixes the underlying clock to the given instant - for tests only.
     * <p>
     * Examples of strings accepted:
     * <ul>
     *     <li>{@code "2020-09-01 12:00Z"}</li>
     *     <li>{@code "2020-09-01 12:00:00Z"}</li>
     *     <li>{@code "2020-09-01 12:00:00.123Z"}</li>
     *     <li>{@code "2020-09-01 12:00:00.123+03:00"}</li>
     *     <li>{@code "2020-09-01T12:00Z"}</li>
     *     <li>{@code "2020-09-01T12:00:00Z"}</li>
     *     <li>{@code "2020-09-01T12:00:00.123Z"}</li>
     *     <li>{@code "2020-09-01T12:00:00.123-06:00"}</li>
     * </ul>
     *
     * @param instantText The ISO-like string representation of the Instant.
     * @return Instant that will be produced by later calls to now.
     */
    public static Instant setNow(String instantText) {
        Instant now = parseInstant(instantText);
        setClock(Clock.fixed(now, ZoneOffset.UTC));
        return now;
    }

    private static Instant parseInstant(String instantText) {
        DateTimeParseException lastException = null;
        for (DateTimeFormatter format : FORMATS) {
            try {
                return format.parse(instantText, Instant::from);
            } catch (DateTimeParseException e) {
                lastException = e;
            }
        }
        assert lastException != null : "As long as at least one format exists, lastException will not be null";
        throw lastException;
    }

    /**
     * Fixes the underlying clock to the given instant - for tests only.
     * This accepts {@link Instant}s, {@link OffsetDateTime}s, {@link ZonedDateTime}s or any other
     * {@link TemporalAccessor}s that can be converted to an {@code Instant}.
     *
     * @param temporalNow Time to fix the clock to.
     * @return Instant that will be produced by later calls to now.
     * @throws DateTimeException when an Instant cannot be constructed from {@code now}, like from LocalDateTime.
     */
    public static Instant setNow(TemporalAccessor temporalNow) {
        Instant now = Instant.from(temporalNow);
        setClock(Clock.fixed(now, ZoneOffset.UTC));
        return now;
    }

    /**
     * Executes the {code action}, temporarily fixing the clock to {@code temporalNow}.
     * <p>
     * After the action is executed, the clock is restored back to what it was before calling this method.
     *
     * @param temporalNow Time to use during the action.
     * @param action      The action to run.
     * @param <X>         Type of exception thrown by action
     * @throws X when action throws
     * @see #setNow(TemporalAccessor)
     */
    public static <X extends Exception>
    void usingNow(TemporalAccessor temporalNow, ThrowingConsumer<Instant, X> action) throws X {
        Clock old = TimeFactoryUtils.clock;
        try {
            action.accept(setNow(temporalNow));
        } finally {
            setClock(old);
        }
    }

    /**
     * Executes the {code action}, temporarily fixing the clock to {@code temporalNow}.
     * <p>
     * After the action is executed, the clock is restored back to what it was before calling this method.
     *
     * @param textNow Time to use during the action.
     * @param action  The action to run.
     * @param <X>     Type of exception thrown by action
     * @throws X when action throws
     * @see #setNow(String)
     */
    public static <X extends Exception>
    void usingNow(String textNow, ThrowingConsumer<Instant, X> action) throws X {
        usingNow(parseInstant(textNow), action);
    }

    /**
     * Fixes the underlying to current time.
     * <p>
     * This method is a shortcut to {@code TimeFactoryUtils.setNow(TimeFactory.now()}.
     * <p>
     * This method is idempotent - multiple calls to freezeNow have the same effect as calling it only once.
     *
     * @return new fixed time.
     */
    public static Instant freezeNow() {
        return setNow(TimeFactory.now());
    }

    /**
     * Fixes the underlying to current time, truncated to given {@code truncate} precision.
     * <p>
     * This method is a shortcut to {@code TimeFactoryUtils.setNow(TimeFactory.now()}.
     * <p>
     * This method is idempotent - multiple calls to freezeNow with same unit have the same effect as calling it only once.
     *
     * @param truncate Truncates current instant before freezing the time.
     * @return new fixed time.
     */
    public static Instant freezeNow(TemporalUnit truncate) {
        return setNow(TimeFactory.now().truncatedTo(truncate));
    }

    /**
     * Changes the underlying clock instance back to default UTC clock - for tests only.
     * <p>
     * Call this before each test that somehow depends on time.
     */
    public static void reset() {
        setClock(Clock.systemUTC());
    }

    private TimeFactoryUtils() {
        throw new AssertionError("Non-instantiable");
    }

    @FunctionalInterface
    public interface ThrowingConsumer<T, X extends Exception> {
        void accept(T t) throws X;
    }
}
