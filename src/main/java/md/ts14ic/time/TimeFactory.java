package md.ts14ic.time;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Test-friendly time-retrieval functions that don't require any dependency injection.
 * <p>
 * Tests can override current clock/time using {@link TimeFactoryUtils} and its different setter methods.<br>
 * <p>
 * {@code TimeFactory} uses a UTC clock by default, to avoid any unexpected issues like when the server timezone changes,
 * and suddenly everything breaks.
 * <p>
 * To use {@code TimeFactory}, replace your calls to {@code now()} of {@code java.time} classes with calls to {@link #now()},
 * {@link #nowMillis()} etc of the {@code TimeFactory}.<br>
 * For example:
 * <pre>{@code
 *     Instant instant = TimeFactory.now(); // instead of Instant.now();
 *     long now = TimeFactory.nowMillis();  // instead of System.currentTimeMillis();
 *     OffsetDateTime odt = TimeFactory.nowOffset(userOffset); // instead of Instant.now().atOffset(userOffset);
 *     ZonedDateTime zdt = TimeFactory.nowZoned(userTimeZone); // instead of Instant.now().atZone(userTimeZone);
 *     LocalDate today = TimeFactory.today(userOffsetOrZone); // instead of ZoneDateTime.
 *
 *     // As a default value for fields
 *     class Person {
 *         private Instant createdAt = TimeFactory.now();
 *         // getters/setters
 *     }
 * }</pre>
 *
 * @see java.time
 * @see TimeFactoryUtils
 */
public class TimeFactory {

    /**
     * Returns the clock behind this factory.
     *
     * @return The clock behind this factory.
     */
    public static Clock clock() {
        return TimeFactoryUtils.clock();
    }

    /**
     * Returns current server time as an {@link Instant}.
     * <p>
     * Use-cases include timestamping events that happened in the past, timestamping log entries, events that should
     * happen after a specified duration regardless of current server time, benchmarking duration etc.
     * <p>
     * This is not suitable to represent a user-specific time - an Instant knows nothing about DST or any historical
     * changes that occurred or will occur to a time-zone.<br>
     * If you want user-specific time, look into {@link #nowZoned(ZoneId)}.
     *
     * @return Current server time.
     * @see #nowMillis()
     */
    public static Instant now() {
        return Instant.now(clock());
    }

    /**
     * Returns current server time in milliseconds since Unix Epoch. Equivalent to {@code now().toEpochMilli()}.
     * <p>
     * This is identical to {@code now().toEpochMilli()}, but has less precision - an Instant also holds nanoseconds.
     * <p>
     * Use-cases include timestamping events that happened in the past, timestamping log entries, events that should
     * happen after a specified duration regardless of current server time, benchmarking duration etc.
     * <p>
     * This is not suitable to represent a user-specific time - a unix-epoch timestamp bears no information about DST
     * or any historical changes that occurred or will occur in a specific time-zone.<br>
     * If you want user-specific time, look into {@link #nowZoned(ZoneId)}.
     *
     * @return Current server time in milliseconds since Unix Epoch.
     * @see #now()
     */
    public static long nowMillis() {
        return now().toEpochMilli();
    }

    /**
     * Returns current server time as seen from a specific (usually a user's) time-zone.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2020-09-01 12:00:00:
     *     TimeFactory.nowOffset(ZoneId.of("Europe/Berlin")) // returns "2020-09-01 15:00:00 +03:00 [Europe/Berlin]"
     * }</pre>
     * <p>
     * This is identical to {@code now().atZone(zoneId)}.
     * <p>
     * Use-cases include those of a {@link ZonedDateTime}. For example scheduling a future appointment, calendar event,
     * retrieving the current time as seen by the user on his wall clock and other calculations in the domain of
     * a specific user time-zone.<br>
     * <p>
     * If all you need are just some server-time calculations, you're probably fine with {@link #now()}.
     * <p>
     * NOTE: {@code ZoneId} can be a fully-fledged time-zone, or just an instance of {@link ZoneOffset},
     * in which case the returned ZonedDateTime will lack information about any DST or other historical or future offset
     * changes in a specific time-zone.
     *
     * @param zoneId The zoneId to apply to current server time.
     * @return Current server time adjusted by zone.
     * @see #nowZoned(String)
     */
    public static ZonedDateTime nowZoned(ZoneId zoneId) {
        return now().atZone(zoneId);
    }

    /**
     * Returns current server time as seen from a specific (usually a user's) time-zone.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2020-09-01 12:00:00:
     *     TimeFactory.nowOffset("Europe/Berlin") // returns "2020-09-01 15:00:00 +03:00 [Europe/Berlin]"
     * }</pre>
     * <p>
     * This is identical to {@code nowZoned(ZoneId.of(zoneId))}.
     * <p>
     * Use-cases include those of a {@link ZonedDateTime}. For example scheduling a future appointment, calendar event,
     * retrieving the current time as seen by the user on his wall clock and other calculations in the domain of
     * a specific user time-zone.<br>
     * <p>
     * If all you need are just some server-time calculations, you're probably fine with {@link #now()}.
     * <p>
     * NOTE: {@code zoneId} can be a fully-fledged tzdata zoneId (Area/Location), or just a zoneOffset (±HH:mm),
     * in which case the returned ZonedDateTime will lack information about any DST or other historical or future offset
     * changes in a specific time-zone.
     *
     * @param zoneId The zoneId to apply to current server time. See {@link ZoneId#of(String)} for valid values.
     * @return Current server time adjusted by zone with zoneId.
     * @see #nowZoned(ZoneId)
     */
    public static ZonedDateTime nowZoned(String zoneId) {
        return nowZoned(ZoneId.of(zoneId));
    }

    /**
     * Returns current server time as seen from a specific (usually a user's) zone offset.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2020-09-01 12:00:00:
     *     TimeFactory.nowOffset(ZoneOffset.of("+05:30")) // returns "2020-09-01 17:30:00 +05:30"
     * }</pre>
     * <p>
     * This is identical to {@code now().atOffset(offset)}.
     * <p>
     * Use-cases include those of an {@link OffsetDateTime}. For example, passing the timestamp through protocols
     * that don't support a fully-fledged {@link ZonedDateTime}.<br>
     * This class is NOT suitable for user-specific calculations, because it's in a sense a sub-type of an Instant:
     * it lacks information about any DST or other historical or future offset changes in a specific time-zone.<br>
     * If you want calculations in a user's time-zone, see {@link #nowZoned}.<br>
     * <p>
     * NOTE: While OffsetDateTime is ISO-8601 compliant, ZonedDateTime is an extension over ISO-8601.
     * As a result not all network protocols support it.<br>
     * OffsetDateTime and this method exist specifically for the case of limited non-java protocols,
     * that don't support ZonedDateTime. In all other cases, you want your calculations done using ZonedDateTime.
     *
     * @param offset The offset to apply to current server time.
     * @return Current server time adjusted by offset.
     * @see ZoneOffset#of(String)
     */
    public static OffsetDateTime nowOffset(ZoneOffset offset) {
        return now().atOffset(offset);
    }

    /**
     * Returns current server time as seen from a specific (usually a user's) zone offset.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2020-09-01 12:00:00:
     *     TimeFactory.nowOffset("+05:30") // returns "2020-09-01 17:30:00 +05:30"
     * }</pre>
     * <p>
     * This is identical to {@code nowOffset(ZoneOffset.of(offset))}.
     * <p>
     * Use-cases include those of an {@link OffsetDateTime}. For example, passing the timestamp through protocols
     * that don't support a fully-fledged {@link ZonedDateTime}.<br>
     * This class is NOT suitable for user-specific calculations, because it's in a sense a sub-type of an Instant:
     * it lacks information about any DST or other historical or future offset changes in a specific time-zone.<br>
     * If you want calculations in a user's time-zone, see {@link #nowZoned}.<br>
     * <p>
     * NOTE: While OffsetDateTime is ISO-8601 compliant, ZonedDateTime is an extension over ISO-8601.
     * As a result not all network protocols support it.<br>
     * OffsetDateTime and this method exist specifically for the case of limited non-java protocols,
     * that don't support ZonedDateTime. In all other cases, you want your calculations done using ZonedDateTime.
     *
     * @param offsetId The offset id to apply to current server time. See {@link ZoneOffset#of(String)} for valid values.
     * @return Current server time adjusted by offset.
     * @see ZoneOffset#of(String)
     */
    public static OffsetDateTime nowOffset(String offsetId) {
        return nowOffset(ZoneOffset.of(offsetId));
    }

    /**
     * Returns current date as seen from a specific (usually a user's) zone region/offset.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2021-12-31T23:00:00Z
     *     LocalDate today = TimeFactory.today(ZoneId.of("Europe/Berlin")); // returns "2022-01-01"
     * }</pre>
     * Use-cases include notifying a user about some event, like their birthday for example.
     *
     * @param zoneId The zone region/offset to apply to current server time.
     * @return Current date at given zone region/offset
     */
    public static LocalDate today(ZoneId zoneId) {
        return nowZoned(zoneId).toLocalDate();
    }

    /**
     * Returns current date as seen from a specific (usually a user's) zone region/offset.
     * <p>
     * This method is a shortcut for {@link #today(ZoneId)}.
     * <p>
     * For example:<pre>{@code
     *     // Current UTC time is 2021-12-31T23:00:00Z
     *     LocalDate today = TimeFactory.today("Europe/Berlin"); // returns "2022-01-01"
     * }</pre>
     * Use-cases include notifying a user about some event, like their birthday for example.
     *
     * @param zoneId The zone region/offset to apply to current server time.
     * @return Current date at given zone region/offset
     */
    public static LocalDate today(String zoneId) {
        return today(ZoneId.of(zoneId));
    }

    private TimeFactory() {
        throw new AssertionError("Non-instantiable");
    }
}
